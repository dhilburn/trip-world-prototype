﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float moveSpeed;

    [HideInInspector] public Facing direction;
    Rigidbody2D playerBody;
    PlayerAttack playerAttackScript;

    bool moveLeft,
         moveRight,
         moveUp,
         moveDown;

    // Parameters
    const string P_RIGHT = "playerMoveRight";
    const string P_LEFT = "playerMoveLeft";
    const string P_UP = "playerMoveUp";
    const string P_DOWN = "playerMoveDown";
    const string P_SPEED = "playerMoveSpeed";
    public Animator playerAnim;

    bool facingRight = true;

    void Awake()
    {
        playerBody = GetComponent<Rigidbody2D>();
        playerAttackScript = GetComponent<PlayerAttack>();
        moveLeft = moveRight = moveUp = moveDown = false;
        playerAnim = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start ()
    {
        direction = Facing.DOWN;
        //InvokeRepeating("GetScreenSize", 0, 1);
	}

    void Update()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        #region Touch Movement
        if (moveRight && !playerAttackScript.attacking)
            MoveRight();
        if (moveLeft && !playerAttackScript.attacking)
            MoveLeft();
        if (moveUp && !playerAttackScript.attacking)
            MoveUp();
        if (moveDown && !playerAttackScript.attacking)
            MoveDown();
        #endregion

        #region Keyboard Movement
        if (RightMovementKeyDown() && !playerAttackScript.attacking)
            MoveRight();
        if (LeftMovementKeyDown() && !playerAttackScript.attacking)
            MoveLeft();
        if (UpMovementKeyDown() && !playerAttackScript.attacking)
            MoveUp();
        if (DownMovementKeyDown() && !playerAttackScript.attacking)
            MoveDown();
        #endregion

        // play animation of movements

        if ((Mathf.Abs(Input.GetAxis("Horizontal")) > 0) || moveLeft || moveRight)
        {
            //float hMvmt = Input.GetAxis("Horizontal");

            float hMvmt = 0;
            if (!(moveLeft || moveRight))
                hMvmt = Input.GetAxis("Horizontal");            
            else if (moveRight)
                hMvmt = 1;
            else if (moveLeft)
                hMvmt = -1;

            playerAnim.SetFloat(P_SPEED, Mathf.Abs(hMvmt));

            if (hMvmt > 0 & !facingRight)
            {
                FlipFacing();
            }
            else if (hMvmt < 0 & facingRight)
                FlipFacing();
        }      
        else if ((Mathf.Abs(Input.GetAxis("Vertical")) > 0) || moveUp || moveDown)
        {
            //float vMvmt = Input.GetAxis("Vertical");

            float vMvmt = 0;
            if (!(moveUp || moveDown))
                vMvmt = Input.GetAxis("Vertical");
            else if (moveUp)
                vMvmt = 1;
            else if (moveDown)
                vMvmt = -1;

            playerAnim.SetFloat(P_SPEED, Mathf.Abs(vMvmt));

            if (vMvmt > 0 & !facingRight)
            {
                FlipFacing();
            }
            else if (vMvmt < 0 & facingRight)
                FlipFacing();
        }
        else
            playerAnim.SetFloat(P_SPEED, 0);

    }


    void GetScreenSize()
    {
        Debug.Log("Screen size: " + Screen.width + " x " + Screen.height);

    }

    string GetFacing()
    {
        switch (direction)
        {
            case Facing.UP:
                return "up";
            case Facing.DOWN:
                return "down";
            case Facing.LEFT:
                return "left";
            case Facing.RIGHT:
                return "right";
            default:
                return "null direction";
        }
    }

    #region Movement Functions

    #region Mobile Checks

    #region Move Left
    public void LeftButtonDown()
    {
        moveLeft = true;
    }

    public void LeftButtonUp()
    {
        moveLeft = false;
    }
    #endregion

    #region Move Right
    public void RightButtonDown()
    {
        moveRight = true;
    }

    public void RightButtonUp()
    {
        moveRight = false;
    }
    #endregion

    #region Move Up
    public void UpButtonDown()
    {
        moveUp = true;
    }

    public void UpButtonUp()
    {
        moveUp = false;
    }
    #endregion

    #region Move Down
    public void DownButtonDown()
    {
        moveDown = true;
    }

    public void DownButtonUp()
    {
        moveDown = false;
    }
    #endregion

    #endregion

    #region Button Checks
    #region Move Right
    bool RightMovementKeyDown()
    {
        return Input.GetKey(KeyCode.D)
            || Input.GetKey(KeyCode.RightArrow);
    }

    public bool RightMovementButtonDown()
    {
        return true;
    }
    #endregion

    #region Move Left
    bool LeftMovementKeyDown()
    {
        return Input.GetKey(KeyCode.A)
            || Input.GetKey(KeyCode.LeftArrow);
    }

    public bool LeftMovementButtonDown()
    {
        return true;
    }
    #endregion

    #region Move Up
    bool UpMovementKeyDown()
    {
        return Input.GetKey(KeyCode.W)
            || Input.GetKey(KeyCode.UpArrow);
    }

    public bool UpMovementButtonDown()
    {
        return true;
    }
    #endregion

    #region Move Down
    bool DownMovementKeyDown()
    {
        return Input.GetKey(KeyCode.S)
            || Input.GetKey(KeyCode.DownArrow);
    }

    public bool DownMovementButtonDown()
    {
        return true;
    }
    #endregion
    #endregion

    #region Movements

    void MoveRight()
    {
        direction = Facing.RIGHT;
        playerBody.velocity = new Vector3(moveSpeed, playerBody.velocity.y);

       
    }

    void MoveLeft()
    {
        direction = Facing.LEFT;
        playerBody.velocity = new Vector3(-moveSpeed, playerBody.velocity.y);

       
    }

    void MoveUp()
    {
        direction = Facing.UP;
        playerBody.velocity = new Vector3(playerBody.velocity.x, moveSpeed);
    }

    void MoveDown()
    {
        direction = Facing.DOWN;
        playerBody.velocity = new Vector3(playerBody.velocity.x, -moveSpeed);
    }

    void FlipFacing()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    #endregion

    #endregion

    
}
