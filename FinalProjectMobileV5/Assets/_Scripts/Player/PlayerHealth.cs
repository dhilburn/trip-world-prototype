﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [Tooltip("Designer editable health the player starts out with.")]
    public int playerHealth;
    [Tooltip("Time that the player is on mercy invincibility until they take damage again")]
    public float mercyInvulTime;

    [SerializeField] int health;
    Color playerColor;
    bool mercyInvul;

	// Use this for initialization
	void Start ()
    {
        health = playerHealth;
        playerColor = GetComponent<SpriteRenderer>().color;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag.Equals("Enemy") && !mercyInvul)
        {
            ChangeHealth(-5);
            StartCoroutine("MercyInvincibility");
        }
    }

	void ChangeHealth(int amount)
    {
        health += amount;
    }

    IEnumerator MercyInvincibility()
    {
        mercyInvul = true;
        yield return new WaitForSeconds(mercyInvulTime);
        mercyInvul = false;
    }

    void OnGUI()
    {
        GUI.color = Color.black;
        GUILayout.BeginArea(new Rect(Screen.width - 100, 0, 100, 24));
        GUILayout.Label("Health " + health);
        GUILayout.EndArea();
    }
}
