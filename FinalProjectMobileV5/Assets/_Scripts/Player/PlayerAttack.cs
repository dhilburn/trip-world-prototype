﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject attackBox;
    public float attackDurationTime;
    public float attackCooldown;

    PlayerMove playerMoveScript;
    GameObject activeAttackBox;
    [HideInInspector] public bool attacking;

    Vector3 UP_WEAPON_POS,
            DOWN_WEAPON_POS,
            LEFT_WEAPON_POS,
            RIGHT_WEAPON_POS,
            UP_CURVE,
            DOWN_CURVE,
            LEFT_CURVE,
            RIGHT_CURVE;            

	// Use this for initialization
	void Start ()
    {
        attacking = false;
        playerMoveScript = GetComponent<PlayerMove>();
        UpdateWeaponPositions();
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateWeaponPositions();
		if(Input.GetKeyDown(KeyCode.Space))
        {
            SetupAttack();            
        }
	}

    void UpdateWeaponPositions()
    {
        UP_WEAPON_POS = new Vector3(transform.position.x, transform.position.y + ((Mathf.Abs(transform.localScale.y) + Mathf.Abs(attackBox.transform.localScale.y)) / 2));
        DOWN_WEAPON_POS = new Vector3(transform.position.x, transform.position.y - ((Mathf.Abs(transform.localScale.y) + Mathf.Abs(attackBox.transform.localScale.y)) / 2));
        LEFT_WEAPON_POS = new Vector3(transform.position.x - ((Mathf.Abs(transform.localScale.x) + Mathf.Abs(attackBox.transform.localScale.y)) / 2), transform.position.y);
        RIGHT_WEAPON_POS = new Vector3(transform.position.x + ((Mathf.Abs(transform.localScale.x) + Mathf.Abs(attackBox.transform.localScale.y)) / 2), transform.position.y);

        UP_CURVE = new Vector3(transform.position.x + ((Mathf.Abs(transform.localScale.x) + Mathf.Abs(attackBox.transform.localScale.y)) / 2), transform.position.y + ((Mathf.Abs(transform.localScale.y) + Mathf.Abs(attackBox.transform.localScale.y)) / 2));
        DOWN_CURVE = new Vector3(transform.position.x - ((Mathf.Abs(transform.localScale.x) + Mathf.Abs(attackBox.transform.localScale.y)) / 2), transform.position.y - ((Mathf.Abs(transform.localScale.y) + Mathf.Abs(attackBox.transform.localScale.y)) / 2));
        LEFT_CURVE = new Vector3(transform.position.x - ((Mathf.Abs(transform.localScale.x) + Mathf.Abs(attackBox.transform.localScale.y)) / 2), transform.position.y + ((Mathf.Abs(transform.localScale.y) + Mathf.Abs(attackBox.transform.localScale.y)) / 2));
        RIGHT_CURVE = new Vector3(transform.position.x + ((Mathf.Abs(transform.localScale.x) + Mathf.Abs(attackBox.transform.localScale.y)) / 2), transform.position.y - ((Mathf.Abs(transform.localScale.y) + Mathf.Abs(attackBox.transform.localScale.y)) / 2));
    }

    public void SetupAttack()
    {
        if (!attacking)
        {
            Vector3 startPos,
                    endPos,
                    curvePos;
            float startAngle,
                  endAngle;

            switch (playerMoveScript.direction)
            {
                case (Facing.UP):
                    startPos = RIGHT_WEAPON_POS;
                    endPos = UP_WEAPON_POS;
                    curvePos = UP_CURVE;
                    startAngle = 270;
                    endAngle = 360;
                    break;
                case (Facing.DOWN):
                    startPos = LEFT_WEAPON_POS;
                    endPos = DOWN_WEAPON_POS;
                    curvePos = DOWN_CURVE;
                    startAngle = 90;
                    endAngle = 180;
                    break;
                case (Facing.LEFT):
                    startPos = UP_WEAPON_POS;
                    endPos = LEFT_WEAPON_POS;
                    curvePos = LEFT_CURVE;
                    startAngle = 0;
                    endAngle = 90;
                    break;
                case (Facing.RIGHT):
                    startPos = DOWN_WEAPON_POS;
                    endPos = RIGHT_WEAPON_POS;
                    curvePos = RIGHT_CURVE;
                    startAngle = 180;
                    endAngle = 270;
                    break;
                default:
                    startPos = Vector3.zero;
                    endPos = Vector3.zero;
                    curvePos = Vector3.zero;
                    startAngle = 0;
                    endAngle = 0;
                    break;
            }
            activeAttackBox = Instantiate(attackBox, startPos, Quaternion.Euler(0, 0, startAngle));
            activeAttackBox.name = "Player Attack";
            activeAttackBox.tag = "Player";
            activeAttackBox.transform.parent = transform;
            StartCoroutine(AnimateAttack(startPos, endPos, curvePos, startAngle, endAngle));
        }
    }

    IEnumerator AnimateAttack(Vector3 startPos, Vector3 endPos, Vector3 curvePos, float startAngle, float endAngle)
    {
        float step = 0;
        attacking = true;
        while (step < attackDurationTime)
        {
            Vector3 endPoint = GetPoint(startPos, endPos, curvePos, (step / attackDurationTime));
            activeAttackBox.transform.position = Vector3.Lerp(activeAttackBox.transform.position, endPoint, (step / attackDurationTime));
            activeAttackBox.transform.rotation = Quaternion.Slerp(Quaternion.Euler(0, 0, startAngle), Quaternion.Euler(0, 0, endAngle), (step / attackDurationTime));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
            activeAttackBox.transform.position = endPoint;
        }
        activeAttackBox.transform.position = endPos;
        activeAttackBox.transform.rotation = Quaternion.Euler(0, 0, endAngle);
        yield return new WaitForSeconds(attackCooldown);
        Destroy(activeAttackBox);
        attacking = false;
    }

    Vector3 GetPoint(Vector3 startPos, Vector3 endPos, Vector3 curvePos, float time)
    {
        time = Mathf.Clamp01(time);
        float oneMinust = 1.0f - time;
        return (oneMinust * oneMinust * startPos) + (2.0f * oneMinust * time * curvePos) + (time * time * endPos);
    }    
}
