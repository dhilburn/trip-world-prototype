﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    public bool showCollisionRays;

    BoxCollider2D thisCollider;

    [HideInInspector]
    public Vector3[] topCollisionPoints;
    [HideInInspector]
    public Vector3[] bottomCollisionPoints;
    [HideInInspector]
    public Vector3[] leftCollisionPoints;
    [HideInInspector]
    public Vector3[] rightCollisionPoints;
    
    float initialX,
          initialY,
          finalX,
          finalY,
          magnitudeX,
          magnitudeY;

    void Awake()
    {
        thisCollider = GetComponent<BoxCollider2D>();
        SetBounds();
        InitializeCollisionPoints();
    }

    void Update()
    {
        SetBounds();

        if (showCollisionRays)
            DrawCollisionRays();
        if (gameObject.tag.Equals(Constants.PLAYER) || gameObject.tag.Equals(Constants.ENEMY) || gameObject.tag.Equals(Constants.NPC))
            UpdateCollisionPoints();
    }

    void SetBounds()
    {
        initialX = thisCollider.bounds.min.x;
        initialY = thisCollider.bounds.min.y;
        finalX = thisCollider.bounds.max.x;
        finalY = thisCollider.bounds.max.y;
        magnitudeX = thisCollider.bounds.size.x;
        magnitudeY = thisCollider.bounds.size.y;
    }

    #region Initialize Collision Point Functions

    void InitializeCollisionPoints()
    {
        topCollisionPoints = new Vector3[11];
        bottomCollisionPoints = new Vector3[11];
        leftCollisionPoints = new Vector3[11];
        rightCollisionPoints = new Vector3[11];

        InitializeTopPoints();
        InitializeBottomPoints();
        InitializeLeftPoints();
        InitializeRightPoints();
    }

    void InitializeTopPoints()
    {
        for (int i = 0; i < 11; i++)
            topCollisionPoints[i] = new Vector3();

        UpdateTopPoints();
    }

    void InitializeBottomPoints()
    {
        for (int i = 0; i < 11; i++)
            bottomCollisionPoints[i] = new Vector3();

        UpdateBottomPoints();
    }

    void InitializeLeftPoints()
    {
        for (int i = 0; i < 11; i++)
            leftCollisionPoints[i] = new Vector3();

        UpdateLeftPoints();
    }

    void InitializeRightPoints()
    {
        for (int i = 0; i < 11; i++)
            rightCollisionPoints[i] = new Vector3();

        UpdateRightPoints();
    }

    #endregion

    #region Update Collision Point Functions
    void UpdateCollisionPoints()
    {
        UpdateTopPoints();
        UpdateBottomPoints();
        UpdateLeftPoints();
        UpdateRightPoints();
    }

    void UpdateTopPoints()
    {
        topCollisionPoints[0] = new Vector3(initialX, finalY);
        for(int i = 1; i < 10; i++)
        {
            topCollisionPoints[i] = new Vector3(initialX + (magnitudeX * (0.1f * i)), finalY);
        }
        topCollisionPoints[10] = new Vector3(finalX, finalY);
    }

    void UpdateBottomPoints()
    {
        bottomCollisionPoints[0] = new Vector3(initialX, initialY);
        for (int i = 1; i < 10; i++)
        {
            bottomCollisionPoints[i] = new Vector3(initialX + (magnitudeX * (0.1f * i)), initialY);
        }
        bottomCollisionPoints[10] = new Vector3(finalX, initialY);
    }

    void UpdateLeftPoints()
    {
        leftCollisionPoints[0] = new Vector3(initialX, initialY);
        for(int i = 1; i < 10; i++)
        {
            leftCollisionPoints[i] = new Vector3(initialX, initialY + (magnitudeY * (0.1f * i)));
        }
        leftCollisionPoints[10] = new Vector3(initialX, finalY);
    }

    void UpdateRightPoints()
    {
        rightCollisionPoints[0] = new Vector3(finalX, initialY);
        for (int i = 1; i < 10; i++)
        {
            rightCollisionPoints[i] = new Vector3(finalX, initialY + (magnitudeY * (0.1f * i)));
        }
        rightCollisionPoints[10] = new Vector3(finalX, finalY);
    }

    #endregion

    #region Check Collision Functions

    public bool CheckCollisionDirection(Facing checkDirection)
    {
        Vector3 castDirection;
        Vector3[] collisionPoints;

        bool collision = false;

        SetCollisionDirectionAndPoints(checkDirection, out castDirection, out collisionPoints);

        if (collisionPoints != null)
        {
            for (int i = 0; i < collisionPoints.Length; i++)
            {
                RaycastHit2D rayHit = Physics2D.Raycast(leftCollisionPoints[i], castDirection, 0.01f, 1 << LayerMask.NameToLayer(Constants.NO_PASS));
                if (rayHit.transform != null)
                {
                    Debug.Log("Collision detected with " + rayHit.transform.gameObject.name);
                    collision = true;
                    break;
                }
            }
        }
        else
        {
            Debug.Log("A collision error has occured");
        }

        return collision;
    }

    void SetCollisionDirectionAndPoints(Facing checkDirection, out Vector3 colDirection, out Vector3[] colPoints)
    {
        switch (checkDirection)
        {
            case Facing.LEFT:
                colDirection = Vector3.left;
                colPoints = leftCollisionPoints;
                break;
            case Facing.RIGHT:
                colDirection = Vector3.right;
                colPoints = rightCollisionPoints;
                break;
            case Facing.UP:
                colDirection = Vector3.up;
                colPoints = topCollisionPoints;
                break;
            case Facing.DOWN:
                colDirection = Vector3.down;
                colPoints = bottomCollisionPoints;
                break;
            default:
                colDirection = Vector3.zero;
                colPoints = null;
                break;
        }
    }

    #endregion

    #region Debug

    void DrawCollisionRays()
    {
        foreach (Vector3 collisionPoint in leftCollisionPoints)
            Debug.DrawRay(collisionPoint, Vector3.left, Color.red, 0.01f, false);
        foreach (Vector3 collisionPoint in rightCollisionPoints)
            Debug.DrawRay(collisionPoint, Vector3.right, Color.red, 0.01f, false);
        foreach (Vector3 collisionPoint in topCollisionPoints)
            Debug.DrawRay(collisionPoint, Vector3.up, Color.red, 0.01f, false);
        foreach (Vector3 collisionPoint in bottomCollisionPoints)
            Debug.DrawRay(collisionPoint, Vector3.down, Color.red, 0.01f, false);
    }

    #endregion
}
