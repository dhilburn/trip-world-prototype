﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    // States
    const string S_WALK_RIGHT = "WalkingRight";
    const string S_WALK_LEFT = "WalkingLeft";
    const string S_WALK_UP = "Walking Up";
    const string S_WALK_DOWN = "WalkingDown";

    // Parameters
    const string P_RIGHT = "playerMoveRight";
    const string P_LEFT = "playerMoveLeft";
    const string P_UP = "playerMoveUp";
    const string P_DOWN = "playerMoveDown";
    const string P_SPEED = "playerMoveSpeed";
    public Animator anim;


    public float speed = 2f;
    public float maxSpeed = 10f;
    public bool right, left, up, down;
    public Rigidbody2D myBody;


	// Use this for initialization
	void Start ()
    {


        anim = GetComponent<Animator>();
	}


	
	// Update is called once per frame
	void Update ()
    {
        MoveChar();
	}

    void MoveChar()
    {
        if(Input.GetKey(KeyCode.D)
            || Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetBool(P_LEFT, false);
            left = false;

            anim.SetBool(P_RIGHT, true);
            right = true;

            anim.SetBool(P_UP, false);
            up = false;

            anim.SetBool(P_DOWN, false);
            down = false;

            transform.Translate(Vector3.right * speed * Time.deltaTime);

            anim.SetFloat(P_SPEED, Mathf.Abs(myBody.velocity.x));

        }

        if (Input.GetKey(KeyCode.A)
            || Input.GetKey(KeyCode.LeftArrow))
        {
            anim.SetBool(P_LEFT, true);
            left = true;

            anim.SetBool(P_RIGHT, false);
            right = false;

            anim.SetBool(P_UP, false);
            up = false;

            anim.SetBool(P_DOWN, false);
            down = false;


            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.W)
            || Input.GetKey(KeyCode.UpArrow))
        {
            anim.SetBool(P_LEFT, false);
            left = false;

            anim.SetBool(P_RIGHT, false);
            right = false;

            anim.SetBool(P_UP, true);
            up = true;

            anim.SetBool(P_DOWN, false);
            down = false;


            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S)
            || Input.GetKey(KeyCode.DownArrow))
        {
            anim.SetBool(P_LEFT, false);
            left = false;

            anim.SetBool(P_RIGHT, false);
            right = false;

            anim.SetBool(P_UP, false);
            up = false;

            anim.SetBool(P_DOWN, true);
            down = true;


            transform.Translate(Vector3.down * speed * Time.deltaTime);
        }

        //// Continue Moving if applicable
        //if (right)
        //    transform.Translate(Vector3.right * speed * Time.deltaTime);
        //if (left)
        //    transform.Translate(Vector3.left * speed * Time.deltaTime);
        //if (up)
        //    transform.Translate(Vector3.up * speed * Time.deltaTime);
        //if (down)
        //    transform.Translate(Vector3.down * speed * Time.deltaTime);
    }
}
