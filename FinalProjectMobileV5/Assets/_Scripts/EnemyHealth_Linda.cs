﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// change health to one for all enemies
public class EnemyHealth_Linda : MonoBehaviour {

    [HideInInspector]
    public float currentHealth;
    [HideInInspector]
    public float myMaxHealth;

    public AudioSource enemySoundSource;

    private int maxHealth;
    private int damageFromFist;
    private int damageFromSword;

    private int myDamageFromSword;
    private bool isDead;
    private Scrollbar myHealthBar;
    private Canvas myHealthDisplay;
    private string myTag;

    private Rigidbody2D myRigidbody;


	// Use this for initialization
	void Start () {

        myTag = gameObject.tag;
        // Debug.Log("My tag is " + myTag);

        // Start all enemies at full health

        currentHealth = myMaxHealth;
        myDamageFromSword = damageFromFist;


        //switch (myTag)
        //{
        //    case "Spider":
        //        currentHealth = spiderMaxHealth;
        //        //Debug.Log("My current health is " + currentHealth);
        //        myDamageFromSword = spiderDamageFromFist;
        //        //Debug.Log("myDamageFromSwordHit is " + myDamageFromSwordHit);
        //        break;

        //    case "Bat":
        //        currentHealth = batMaxHealth;
        //        //Debug.Log("My current health is " + currentHealth);
        //        myDamageFromSword = batDamageFromFist;

        //        break;

        //    case "Rat":
        //        currentHealth = ratMaxHealth;
        //        //Debug.Log("My current health is " + currentHealth);
        //        myDamageFromSword = ratDamageFromFist;

        //        break;
        //}
        

        myHealthBar = GetComponentInChildren<Scrollbar>();
        myHealthDisplay = GetComponentInChildren<Canvas>();
        myHealthDisplay.enabled = false;

        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update () {
        if(currentHealth < myMaxHealth)
        {
            myHealthDisplay.enabled = true;
        }
        HealthStatus();
        myHealthBar.size = currentHealth / myMaxHealth;
	}

    // If the sword enters the enemy collider
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Sword"))
        {
            SwordHit(myDamageFromSword);
            
        }
    }

    void HealthStatus()
    {
        if (currentHealth <= 0)  // Check to see if Enemy is DEAD
        {

            isDead = true;
            currentHealth = 0;
            myHealthDisplay.enabled = false;

            //myRigidbody.constraints = RigidbodyConstraints2D.None;
            //Ray rayPush = Camera.main.ScreenPointToRay(Input.mousePosition);

            //myRigidbody.AddForce(rayPush.direction.normalized * 10.0f, ForceMode.Impulse);
            Destroy(gameObject, 5.0f);
        }   
    }

    public void SwordHit(int swordDamage)
    {
        currentHealth -= swordDamage;

        // generate audio with damage
        enemySoundSource.Play();
        enemySoundSource.loop = false;

    }

    public void FistHit(float fistDamage)
    {
        currentHealth -= fistDamage;

    }

}
