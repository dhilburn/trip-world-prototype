﻿// This script is used to control the background music and sound effects through
// all levels in the game.
// It needs to be added to an empty game object in the very first scene
// Can be used to call the PlayAudio() function in other scripts
//  with the command SingletonAudioSource.Instance.PlayAudio(SingletonAudioSource.Instance.soundClip);

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AudioManagerSingleton : MonoBehaviour
{

    //  Buttons
    public AudioClip buttonSelect;
    public AudioSource buttonSource;

    // Background Music
    AudioSource backgroundSource;

    // Sounds used every level
    public AudioSource enemyAttackSource;
    public AudioSource enemyDieSource;

    AudioSource breakCrateSource;
    AudioSource breakBarrelSource;
    public AudioSource healthPickupSource;

    public AudioSource playerDeathSource;

    // Static singleton variable
    public static AudioManagerSingleton Instance { get; private set; }

    // Singleton Housekeeping
    // We want this to happen as soon as the game first takes breath!
    void Awake()
    {

        // Check if there are any other instances conflicting with this script's instance
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        // Save a reference to the SingletonAudioSource component as the singleton instance
        Instance = this;

        // Make sure this object is not destroyed between scenes
        DontDestroyOnLoad(gameObject);
    }

    //Instance method: PlayBackground ()
    // Plays background music in each scene
    // This method can be accessed through the singleton Instance
    public void PlayBackground(AudioClip music)
    {
        backgroundSource.clip = music;
        backgroundSource.Play();
        backgroundSource.loop = true;
    } // end PlayBackground

    // Instance method: StopBackground()
    // Stops background music
    public void StopBackground()
    {
        backgroundSource.Stop();
    }

    public void PlayEffect(int effect)
    {
        // Keep track of current scene
        Scene currentScene = SceneManager.GetActiveScene();

        switch (effect)
        {
            case 1:         // button clicky noise
                buttonSource.clip = buttonSelect;
                buttonSource.Play();
                buttonSource.loop = false;
                break;

            case 2:         //  enemy attack noises 
                if (AudioManagerSingleton.Instance != null)
                {
                    enemyAttackSource.clip = Resources.Load<AudioClip>("Audio/Soldier_Attack");
                    enemyAttackSource.Play();
                    enemyAttackSource.loop = false;
                }
                break;



            case 4:         //  enemy death noises
                if (AudioManagerSingleton.Instance != null)
                {
                    enemyAttackSource.clip = Resources.Load<AudioClip>("Audio/Soldier_Die");
                    enemyAttackSource.Play();
                    enemyAttackSource.loop = false;
                }
                break;

            case 6:         // Health Pickup noise
                healthPickupSource.clip = Resources.Load<AudioClip>("Audio/Health_Pickup");
                healthPickupSource.Play();
                healthPickupSource.loop = false;
                break;

            case 7:         // Break Crate use noise
                breakCrateSource.clip = Resources.Load<AudioClip>("Audio/Break_Crate");
                breakCrateSource.Play();
                breakCrateSource.loop = false;
                break;

            case 8:         // Break Barrel use noise
                breakBarrelSource.clip = Resources.Load<AudioClip>("Audio/Break_Barrel");
                breakBarrelSource.Play();
                breakBarrelSource.loop = false;
                break;



            case 11:         // the player death noise 
                playerDeathSource.clip = Resources.Load<AudioClip>("Audio/Player_Death");
                playerDeathSource.Play();
                playerDeathSource.loop = false;
                break;

            default:
                break;
        }
    } // end PlayEffect()
}
