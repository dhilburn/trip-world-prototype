﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUDController : MonoBehaviour
{


    //Set up the variable for the health bar scroller
    public Scrollbar healthBar;
    

    public static HUDController Instance;



    void Awake()
    {
        // Reference the transform
        healthBar = healthBar.GetComponent<Scrollbar>();
       
    }
    // Use this for initialization
    void Start()
    {

        Instance = this;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateHUD(float percentHealth)
    {
        healthBar.size = percentHealth;
      

    }

  
}

