﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;


public class LoadXMLLevel : MonoBehaviour
{

    const string BACKGROUND_FLOOR = "Background Floor";
    const string BACKGROUND = "Background";
    const string BACKGROUND_DETAIL = "Background Details";
    const string OBSTACLE = "Obstacles";
    const string INTERACTABLE = "Interactable";
    const string PLAYER = "Player";
    const string FOREGROUND = "Foreground";
    const string FOREGROUND_DETAIL = "Foreground Details";

    public TextAsset levelInfo;  //added

    // Use this for initialization
    void Start()
    {
        // make an xml doc
        XmlDocument xmlDoc = new XmlDocument();
        // TextAsset levelInfo = new TextAsset();
        // levelInfo = Resources.Load("Levels/testLevel") as TextAsset;


         //Debug.Log(levelInfo.text);


        //loaded the xml into the XmlDocument
        xmlDoc.LoadXml(levelInfo.text);

        // Ge the name of the image that holds the title
        //string nameOfImage = xmlDoc.DocumentElement.ChildNodes[0].ChildNodes[0].Attributes["source"].InnerText.Split('.')[0];  // changed to get rid of file extension

        string nameOfImage = xmlDoc.DocumentElement.ChildNodes[0].Attributes["name"].InnerText;

        Debug.Log(nameOfImage);

        // Get all the sprites in the image and store in List or array
        Sprite[] levelSprites = Resources.LoadAll<Sprite>(nameOfImage);
        Debug.Log(levelSprites.Length);

        // Get all teh layer names for all the layers by
        // searching node names using a string
        XmlNodeList layers = xmlDoc.DocumentElement.GetElementsByTagName("layer");
        // Debug.Log(layers.Count);

        //TODO: Get the tile number for all the properties in this tile sheet and store them in an int LIST
        //TODO: Store the XML node that is connected to the tile in a parallel list
        //when you add the tile number to specialTileNumber, add the node to specialTileNode
        //You can use a dictionary instead of parallel list if you wanted
        List<int> specialTileNumber = new List<int>();
        List<XmlNode> specialTileNode = new List<XmlNode>();

        

        // Need height and width of each tile
        float tileWidth = float.Parse(xmlDoc.DocumentElement.ChildNodes[0].Attributes["tilewidth"].InnerText);
        float tileHeight = float.Parse(xmlDoc.DocumentElement.ChildNodes[0].Attributes["tileheight"].InnerText);
        Debug.Log(tileWidth + "  " + tileHeight);

        //Resources.Load(nameOfImage)

        GameObject emptyObject = new GameObject();  // Create a new empty game object


        for (int layerNum = 0; layerNum < layers.Count; layerNum++) //foreach layer
        {
            // Create objects for layer 1
            GameObject parentObject = new GameObject();
            parentObject.name = layers[layerNum].Attributes["name"].InnerText;

            // Go through the FIRST layer (Background Floor layer)
            float layerWidth = float.Parse(layers[layerNum].Attributes["width"].InnerText);
            float layerHeight = float.Parse(layers[layerNum].Attributes["height"].InnerText);

            // Select the data node by searching using a string
            XmlNode dataNode = layers[layerNum].SelectSingleNode("data");

            // Debug.Log(dataNode.InnerText);

            // First and last is empty e.g. row[0] and row.Length are empty
            string[] rows = dataNode.InnerText.Split('\n');  // these are the rows, ex. 275, 272, 275, 211

            //Debug.Log(rows.Length);
            //Debug.Log(rows[0]);


            for (int rowCounter = 1; rowCounter < layerHeight + 1; rowCounter++)   // for each column (275, 273, 274)
            {
                // Get the row located by the column
                string[] values = rows[rowCounter].Split(',');  // Split the row into individual values
                                                         // Debug.Log(values[0]);

                // go through each individual tile value
                for (int singleValue = 0; singleValue < layerWidth; singleValue++)  // for each individual value in a row
                {
                    int theIndexOfTheSprite = int.Parse(values[singleValue]);

                    // Debug.Log(values[singleValue]);
                    if (theIndexOfTheSprite == 0)
                        continue;



                    GameObject tempObj = Instantiate(emptyObject);
                    tempObj.name = layers[layerNum].Attributes["name"].InnerText;
                    tempObj.transform.SetParent(parentObject.transform);

                    tempObj.transform.position = new Vector3(singleValue, layerHeight - rowCounter, 0f);

                    SpriteRenderer tempRenderer = tempObj.AddComponent<SpriteRenderer>();
                    //Debug.Log("Index of Sprite " + theIndexOfTheSprite);

                    tempRenderer.sprite = levelSprites[theIndexOfTheSprite-1];
                    tempRenderer.sortingLayerName = layers[layerNum].Attributes["name"].InnerText;

                    // Apply components based on layer
                    if (layers[layerNum].Attributes["name"].InnerText == "Obstacles")
                    {
                        Rigidbody2D tempRB = tempObj.AddComponent<Rigidbody2D>();
                        tempRB.gravityScale = 0;
                        tempRB.simulated = false;
                        tempObj.AddComponent<BoxCollider2D>();
                    }

                    //TODO: Apply components based on special tiles (properties)
                    //if(specialTileNumber.Contains(theIndexOfTheSprite))
                    //{
                        //Find out what type of special tile it is using the xmlNodeList of special tiles to parse type
                            //parse the relevant information from there
                            //add components and alter the components 
                    //}

                    // same as this
                    //if(parentObject.name == "Obstacle")
                    //{

                    //}


                }// end of ech individual value in a row
            } // end for 


        } // end for each layer





    }

    // Update is called once per frame
    void Update()
    {

    }
}
