﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonControls : MonoBehaviour {

    public void _PlayButton (string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void _CreditsButton(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void _HelpButton(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void _QuitButton()
    {
        Application.Quit();
    }
}
