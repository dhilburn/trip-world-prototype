﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{

    Scene currentScene;
    void Start()
    {
        currentScene = SceneManager.GetActiveScene();
    }

    // Update is called once per frame
    void Update()
    {

        if (currentScene.name == "00_StartScene")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                SceneManager.LoadScene("01_SplashScene");
            }
        }
        else if (currentScene.name == "01_SplashScene")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                SceneManager.LoadScene("02_MainMenu");
            }
        }
        else if (currentScene.name == "06_Instructions")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                SceneManager.LoadScene("02_MainMenu");
            }
        }
        else if (currentScene.name == "04_WinScene")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                SceneManager.LoadScene("02_MainMenu");
            }
        }
        else if (currentScene.name == "05_LoseScene")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                SceneManager.LoadScene("02_MainMenu");
            }
        }

        else if (currentScene.name == "03_Credits")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                SceneManager.LoadScene("02_MainMenu");
            }
        }
       
    }
}
