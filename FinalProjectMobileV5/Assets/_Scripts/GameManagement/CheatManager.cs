﻿

// CheatManager is put on an Empty Game Object in each scene
// Public variable "nextScene" requires entering the name of the scnen after this you wish to go to

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CheatManager : MonoBehaviour
{

    LevelManager cheatLevelManager;

    public string nextScene;

    // Use this for initialization
    void Start()
    {

        // Set up easy access to the Level Manager script
        cheatLevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        // Keep track of the current scene
        //Scene currentScene = SceneManager.GetActiveScene();
    }

    // Update is called once per frame
    void Update()
    {
        // Advance scenes with N key
        if (Input.GetKeyDown(KeyCode.N))
        {
            Debug.Log("Advancing to next screen.");
            SceneManager.LoadScene(nextScene);
        }

        //// Decrease player health - key
        //if (Input.GetKeyDown(KeyCode.KeypadMinus))
        //{
        //    cheatLevelManager.LoseHealth(10);
        //    Debug.Log("Player lost 10 hp");
        //}

        //// Increase player health with + key
        //if (Input.GetKeyDown(KeyCode.KeypadPlus))
        //{
        //    cheatLevelManager.AddHealth(10);
        //    Debug.Log("Player gained 10 hp");
        //}

    }
}
