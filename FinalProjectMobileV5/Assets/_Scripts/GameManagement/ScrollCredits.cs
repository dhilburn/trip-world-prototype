﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollCredits : MonoBehaviour
{

    public float moveSpeed = 10f;
    private Transform ThisTransform = null;

    void Awake()
    {
        ThisTransform = GetComponent<Transform>();
    }

	
	// Update is called once per frame
	void Update ()
    {
        ThisTransform.position += ThisTransform.up * moveSpeed * Time.deltaTime;
	}
}
