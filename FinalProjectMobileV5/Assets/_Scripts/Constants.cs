﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Facing
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public enum Behaviour
{
    WAIT,
    WANDER,
    CHASE,
    FLEE
}

public class Constants
{
    public const string ENVIRONMENT = "Environment";
    public const string PLAYER = "Player";
    public const string ENEMY = "Enemy";
    public const string NPC = "NPC";
    public const string PICKUP = "Pickup";
    public const string NO_PASS = "Non-Passable";
    public const string ATTACK = "Attack Box";
}
